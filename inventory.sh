#!/bin/bash

case "$equip" in
linux)
	if [ "$1" == "--list" ] ; then
	cat<<EOF
	{
	  "bash_hosts": {
		"hosts": [
		  "linux1.local",
		  "linux2.local"
		],
		"vars": {
		  "host_proxy_var": "proxy2"
		}
	  },
	  "_meta": {
		"hostvars": {
		  "linux1.local": {
			"host_specific_var": "apache"
		  },
		  "linux2.local": {
			"host_specific_var": "test"
		  }
		}
	  }
	} 
EOF
	elif [ "$1" == "--host" ]; then
	  echo '{"_meta": {"hostvars": {}}}'
	else
	  echo "{ }"
	fi
	;;
	
windows)
	if [ "$1" == "--list" ] ; then
	cat<<EOF
	{
	  "bash_hosts": {
		"hosts": [
		  "windows1.local",
		  "windows2.local"
		],
		"vars": {
		  "host_proxy_var": "proxy2"
		}
	  },
	  "_meta": {
		"hostvars": {
		  "windows1.local": {
			"host_specific_var": "iis"
		  },
		  "windows2.local": {
			"host_specific_var": "test"
		  }
		}
	  }
	} 
EOF
	elif [ "$1" == "--host" ]; then
	  echo '{"_meta": {"hostvars": {}}}'
	else
	  echo "{ }"
	fi
	;;
	
*)
	if [ "$1" == "--list" ] ; then
	cat<<EOF
	{
	  "bash_hosts": {
		"hosts": [
		  "aix1.local",
		  "aix2.local"
		],
		"vars": {
		  "host_proxy_var": "proxy2"
		}
	  },
	  "_meta": {
		"hostvars": {
		  "aix1.local": {
			"host_specific_var": "oracle"
		  },
		  "aix2.local": {
			"host_specific_var": "test"
		  }
		}
	  }
	} 
EOF
	elif [ "$1" == "--host" ]; then
	  echo '{"_meta": {"hostvars": {}}}'
	else
	  echo "{ }"
	fi
	;;
esac